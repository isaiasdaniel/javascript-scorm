var scorm = pipwerks.SCORM;

var gradeCount = 0.0;

function init(){
	//Specify SCORM 1.2:
	scorm.version = "1.2";
	scorm.init();
}

function complete(grade){
	scorm.set("cmi.core.score.raw", gradeCount);
	scorm.save();
	scorm.set("cmi.core.lesson_status", "completed");
	scorm.quit();
}

window.onload = function (){
	init();	
}

/* Procada ejecicion estas deben ser iniciaizadas antes de llamar a la funcion grade()*/

//var sol = ['POST','POST','GET','GET','GET','POST','POST','POST'];



//var sol =['M','V','C'];
//	gradeCount = 0.0;

/*************************************************************** 
Funcion Grade util para calificar preguntas de opcion multiple 
como actividad 001 y 002 e imprime un mensaje si es correcta e
incorrecta.

	var1 Numero de pregunta a realizar
	var2 Respuesta seleccionada por el estudiante
	correct Muestra la funcion correcta en caso de ser true
****************************************************************/
function grade(var1,var2,correct){
	var showGrade = document.getElementById("grade");	
	if (sol[var1]===var2){
		gradeCount++;
		showGrade.innerHTML="GRADE : " + ((gradeCount*10.0)/sol.length).toFixed(2);
		var mjs = var2+" is correct answer";
		var askClass= "goodAsk";
	}else{
		var mjs = var2+" is wrong answer";
		var askClass="badAsk";
			if(correct){
				mjs = mjs +" The correct answer is "+sol[var1];
		}
	}
	
	$(".ask"+var1).fadeOut(300, 
		function(){ 
			$(this).remove();
			var askOption = document.getElementById("ask"+var1);
			$("ask"+var1).hide();
			askOption.className=askClass;
			askOption.innerHTML = mjs;
			$("#ask"+var1).show(3000);
		});
}

/*******************************************************************
Funcion gradeFeedBack Para preguntas dentro de una tabla (rellena la 
tabla) con texto de retroalimentacion.

Son necesarios array asociativo del tipo

{"WS": "FeedBack", "WS":"FeedBack" "C":"FeedBack" ....}
*******************************************************************/

var sol = {	C0:"Just before starting the thread.",
			C1:"Then service() method calls doGet() or doPost().",
			C2:"Starts a servlet thread.",
			C3:"Generates the HTTP response stream from the data in response object",
			Ws4:"Uses it to talk to the client browser.",
			S5:"The dynamic content for the client.",
			S6:"Uses it to print a response",
			C6:"Container gives it the servlet",
			C7:"To find the correct servlet for the request",
			C8:"Once the servlet is finished.",
			C9:"Knows the servlet is finished.",
			Ws9:"Wnows how to forward to the Container.",
			C10:"Calls service method (and others you'll see)",
			S11:"public class Whatever"};

var opt = ['Ws','C','S'];

function gradeFeedBack(var1,var2,r){
	var showGrade = document.getElementById("grade");
	if(sol[var2+var1]){
		askClass= "goodAskT";
		gradeCount++;
	}else{
		askClass= "badAskT";
	}
	showGrade.innerHTML="GRADE : " + (gradeCount*10.0/r).toFixed();
	for(o in opt){
			var askOption = document.getElementById(var1+"_"+opt[o]);
			askOption.className = askClass;
			askOption.onclick = "";
			if(sol[opt[o]+var1]){
				askOption = document.getElementById(var1+"_"+opt[o]);
				askOption.innerHTML = sol[opt[o]+var1];
			}
		}
}

/*****************************************************************
 * Verifica si un codigo es correcto
 *****************************************************************/
//var Sol = "<web-app...><servlet><servlet-name>C2dice</servlet-name><servlet-class>Ch2Dice</servlet-class></servlet><servlet-mapping><servlet-name>C2dice</servlet-name><url-pattern>/Dice</url-pattern></servlet-mapping></web-app>";
//var Sol = "importjavax.servlet.*;importjavax.servlet.http.*;importjava.io.*;publicclassCh2DiceextendsHttpServlet{publicvoiddoGet(HttpServletRequestrequest,HttpServletResponseresponse)throwsIOException{PrintWriterout=response.getWriter();Stringd1=Integer.toString((int)((Math.random()*6)+1));Stringd2=Integer.toString((int)((Math.random()*6)+1));out.println(<html><body>+<h1align=center>HF'sChap2DiceRoller</h1>+<p>+d1+and+d2+wererolled+</body></html>);}}";

function gradeCode(){
	var ask = $("#code").text().replace(/\s+|\"/g,"");
	if (ask === Sol){
		gradeCount = 10.0;
	}else{
		gradeCount = 5.0;
	}
	saveGrade();
}


/*****************************************************************
Guarda las respuesta en moodle y finaliza la actividad
*****************************************************************/

function saveGrade(){
	complete();
	$("#fin").dialog("open");
}
