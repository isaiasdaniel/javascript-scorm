
var doc = $(document);
doc.ready(startApp);



function startApp(){
	initDialog();
	initDraggable();
	initButton();
	initDroppable();
	initSortable();
}

$("em").click(function () {
      $("div#"+$(this).attr("id")).show("drop", "fast");
      $(this).attr("id",undefined);
      $(this).attr("class","drop3");
      $(this).text("");
    });

function initDialog(){
	$("#description").dialog({
		title: "Instructions",
		modal: true,
		maxHeight: 200,
		maxWidth: 600,
		minHeight: 150,
		minWidth: 600,
		buttons:{"Ok": function(){$(this).dialog("close")}}
	});
	
	$("#fin").dialog({
		title: "End",
		modal: true,
		autoOpen: false
	});
}

function initButton(){
	$("#Calificar").button();
}

function initDraggable(){
	$(".draggable").draggable({
	helper:"clone",
	cursor: "move"});
	$(".draggable2").draggable();
	$(".draggable3").draggable({
	helper:"clone",
	cursor: "move"});
}

function initDroppable(){
	$("#sortable").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ".draggable",
		drop: function( event, ui ) {
		$( this ).find( ".Instruction" ).remove();
		$( "<div id=\""+ui.draggable.attr("id")+"\"class=\"draggable2\"></div>" ).html( ui.draggable.html() ).appendTo( this );
		ui.draggable.hide("drop", "fast");
		}
	});

	$("#ask").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ".draggable2",
		drop: function( event, ui ) {
			$("div#"+ui.draggable.attr("id")).show("drop", "fast");
			ui.draggable.remove();
			
		}
	});

	$(".drop3").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ".draggable3",
		drop: function( event, ui ) {
			var id=$( this ).attr("id");
			if (id!=undefined){
				$("div#"+id).show("drop", "fast");
			}
			
			$( this ).text(ui.draggable.text())
			ui.draggable.hide("drop", "fast");
			$( this ).attr("id",ui.draggable.attr("id"));
			$( this ).attr("class","text");
		}
	});
}

function initSortable(){
	$("#sortable").sortable();
}
