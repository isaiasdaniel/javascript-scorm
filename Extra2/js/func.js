var scorm = pipwerks.SCORM;

var gradeCount = 0.0;

function init(){
	//Specify SCORM 1.2:
	scorm.version = "1.2";
	scorm.init();
}


function complete(grade){
	scorm.set("cmi.core.score.raw", gradeCount);
	scorm.save();
	scorm.set("cmi.core.lesson_status", "completed");
	scorm.quit();
}

/*****************************************************************
 * Verifica si un codigo es correcto
 *****************************************************************/
var Sol = "<web-app...><context-param><param-name>alumno</param-name><param-value>PedroGonzalez</param-value></context-param><servlet><servlet-name>ParametrosInicio</servlet-name><servlet-class>com.example.web.PruebaParametros</servlet-class><init-param><param-name>message</param-name><param-value>Hello,world!</param-value></init-param></servlet><servlet-mapping><servlet-name>ParametrosInicio</servlet-name><url-pattern>/ch15-1/talktomeplease</url-pattern></servlet-mapping></web-app>";

function gradeCode(){
	var showGrade = document.getElementById("grade");
	var ask = $("#code").text().replace(/\s+|\"/g,"");
	if (ask === Sol){
		showGrade.innerHTML="GRADE : " + 10;
		gradeCount = 10.0;
	}else{
		showGrade.innerHTML="GRADE : " + 1;
		gradeCount = 0.0;
	}
	saveGrade();
}


/*****************************************************************
Guarda las respuesta en moodle y finaliza la actividad
*****************************************************************/

function saveGrade(){
	complete();
	$("#fin").dialog("open");
}

window.onload = function (){
	init();	
}

