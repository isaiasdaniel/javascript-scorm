var scorm = pipwerks.SCORM;

var gradeCount = 0.0;

function init(){
	//Specify SCORM 1.2:
	scorm.version = "1.2";
	scorm.init();
}

function complete(grade){
	scorm.set("cmi.core.score.raw", gradeCount);
	scorm.save();
	scorm.set("cmi.core.lesson_status", "completed");
	scorm.quit();
}

window.onload = function (){
	init();	
}


/*****************************************************************
 * Verifica si un codigo es correcto
 *****************************************************************/
var Sol = "<web-app...><servlet><servlet-name>C2dice</servlet-name><servlet-class>Ch2Dice</servlet-class></servlet><servlet-mapping><servlet-name>C2dice</servlet-name><url-pattern>/Dice</url-pattern></servlet-mapping></web-app>";
//var Sol = "importjavax.servlet.*;importjavax.servlet.http.*;importjava.io.*;publicclassCh2DiceextendsHttpServlet{publicvoiddoGet(HttpServletRequestrequest,HttpServletResponseresponse)throwsIOException{PrintWriterout=response.getWriter();Stringd1=Integer.toString((int)((Math.random()*6)+1));Stringd2=Integer.toString((int)((Math.random()*6)+1));out.println(<html><body>+<h1align=center>HF'sChap2DiceRoller</h1>+<p>+d1+and+d2+wererolled+</body></html>);}}";

function gradeCode(){
	var ask = $("#code").text().replace(/\s+|\"/g,"");
	if (ask === Sol){
		gradeCount = 10.0;
	}else{
		gradeCount = 5.0;
	}
	saveGrade();
}


/*****************************************************************
Guarda las respuesta en moodle y finaliza la actividad
*****************************************************************/

function saveGrade(){
	complete();
	$("#fin").dialog("open");
}
