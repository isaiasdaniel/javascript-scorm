
var doc = $(document);
doc.ready(startApp);



function startApp(){
	initDialog();
	initDraggable();
	initButton();
	initDroppable();
	initSortable();
}

function initDialog(){
	$("#description").dialog({
		title: "Instructions",
		modal: true,
		maxHeight: 200,
		maxWidth: 600,
		minHeight: 150,
		minWidth: 600,
		buttons:{"Ok": function(){$(this).dialog("close")}}
	});	
	
	$("#fin").dialog({
		title: "End",
		modal: true,
		autoOpen: false
	});
}

function initButton(){
	$("#Calificar").button();
}

function initDraggable(){
	$(".draggable").draggable({
	helper:"clone",
	cursor: "move"});
	$(".draggable2").draggable();
}

function initDroppable(){
	$("#sortable").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ".draggable",
		drop: function( event, ui ) {
		$( this ).find( ".Instruction" ).remove();
		$( "<div id=\""+ui.draggable.attr("id")+"\"class=\"draggable2\"></div>" ).html( ui.draggable.html() ).appendTo( this );
		ui.draggable.hide("drop", "fast");
		}
	});

	$("#ask").droppable({
		activeClass: "ui-state-default",
		hoverClass: "ui-state-hover",
		accept: ".draggable2",
		drop: function( event, ui ) {
			$("div#"+ui.draggable.attr("id")).show("drop", "fast");
			ui.draggable.remove();
		}
	});
}

function initSortable(){
	$("#sortable").sortable();
}
