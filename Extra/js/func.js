var scorm = pipwerks.SCORM;

var gradeCount = 0.0;

function init(){
	//Specify SCORM 1.2:
	scorm.version = "1.2";
	scorm.init();
}

function complete(grade){
	scorm.set("cmi.core.score.raw", gradeCount);
	scorm.save();
	scorm.set("cmi.core.lesson_status", "completed");
	scorm.quit();
}

/*****************************************************************
Guarda las respuesta en moodle y finaliza la actividad
*****************************************************************/

function saveGrade(){
	complete();
	$("#fin").dialog("open");
}

/*****************************************************************
 * Verifica si un codigo es correcto
 *****************************************************************/

var Sol = "importjava.io.*;importjava.util.*;importjavax.servlet.*;publicclassIndexPracticasextendsHttpServlet{protectedvoidprocessRequest(HttpServletRequestrequest,HttpServletResponseresponse)throwsServletException,IOException{response.setContentType(text/html);PrintWriterout=response.getWriter();out.println(testsessionattribute<bre>);HttpSessionsession=request.getSession();if(session.isNew()){out.println(Thisisanewsession.);}else{out.println(Welcomeback!);}HashMaprutas=newHashMap();try{Enumeration<String>initParameterNames=getServletConfig().getInitParameterNames();while(initParameterNames.hasMoreElements()){Stringname=initParameterNames.nextElement();Stringvalue=getServletConfig().getInitParameter(name);rutas.put(name,value);}request.setAttribute(rutas,rutas);RequestDispatcherrequestDispatcher=getServletContext().getRequestDispatcher(/menu.jsp);requestDispatcher.forward(request,response);}finally{out.close();}}protectedvoiddoGet(HttpServletRequestrequest,HttpServletResponseresponse)throwsServletException,IOException{processRequest(request,response);}protectedvoiddoPost(HttpServletRequestrequest,HttpServletResponseresponse)throwsServletException,IOException{processRequest(request,response);}}";

function gradeCode(){
	var showGrade = document.getElementById("grade");
	var ask = $("#code").text().replace(/\s+|\"/g,"");
	if (ask === Sol){
		gradeCount = 10.0;
		showGrade.innerHTML="GRADE : " + 10;
		
	}else{
		gradeCount = 0.0;
		showGrade.innerHTML="GRADE : " + 1;
		
	}
	saveGrade();
}

window.onload = function (){
	init();	
}








