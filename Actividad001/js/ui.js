var doc = $(document);
doc.ready(startApp);

function startApp(){
	$("#description").dialog({
		title: "Instructions",
		modal: true,
		buttons:{"Ok": function(){$(this).dialog("close")}}
	});	
	$("a").button();
	$("#fin").dialog({
		title: "Fin",
		modal: true,
		autoOpen: false
	});
	$(".drag").draggable();
	$("#code").droppable({
		accept: ".drag",
		activeClass: "ui-state-hover",
		hoverClass: "ui-state-active",
		drop:function( event, ui ) {
				 $(this).append(ui.draggable.html());
				 ui.draggable.fadeOut('slow', function() {
					    $(this).remove();
					  });
					 
	}
	});
}
